// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


getThreadIdFromUrl = (url) => {
    let match = url.match(/.*\/t(\d+)-.*html$/);
    if (match) {
        return match[1];
    }
};

determineDefaultFolder = () =>{

    let defaultFolder = "";
    let threadTitle = "";
    let threadId = "";
    if (document.URL.match(".*/t[0-9]+-.*\.html")){
        let navbar = document.querySelector("td.navbar");
        if (navbar) {
            defaultFolder = navbar.innerText.trim();
            threadTitle = defaultFolder;
        }
        threadId = getThreadIdFromUrl(document.URL);
        let match = document.URL.match(".*/t[0-9]+-p([0-9]+)-.*.html");
        if (match) {
            if (navbar) {
                defaultFolder =  defaultFolder + " - " + match[1];
            }
        }
    } else if (document.URL.match(/\/showthread.php\?/)) {
        let navbar = document.querySelector("td.navbar");
        if (navbar) {
            defaultFolder = navbar.innerText.trim();
            threadTitle = defaultFolder;
        }
        let pagenav = document.querySelector("div.pagenav");
        if (pagenav) {
            let linkToFirstPage = pagenav.querySelector("a").href;
            threadId = getThreadIdFromUrl(linkToFirstPage);
        }
    } else if (document.URL.match(".*/showpost.php\\?p=([0-9]+).*")) {
        let match = document.URL.match(".*/showpost.php\\?p=([0-9]+).*");
        if (match){
            let threadTitle = document.querySelector("td.tcat a");
            if (threadTitle) {
                defaultFolder = threadTitle.innerText.trim() + " - ";
            }
            defaultFolder += match[1];
            let postTitle = document.querySelector("table:nth-of-type(2) div.smallfont strong");
            if (postTitle) {
                defaultFolder += " - " + postTitle.innerText.trim();
            }
        }
    } else if (document.URL.match('https?://(www\\.)?imagefap\\.com')){
        let e = document.querySelector("div#menubar table table td b");
        if (e) {
            defaultFolder = e.innerText;
        }
    } else if (document.URL.match('https?://xhamster\\.com/photos/gallery/')) {
        let e = document.querySelector("div.page-title h1");
        if (e) {
            defaultFolder = e.innerText.trim();
            console.log(defaultFolder)
        }
    }

    defaultFolder = defaultFolder.replace(/([^-a-zA-Z0-9 ()]+)/gi, '-');
    browser.runtime.sendMessage({ defaultFolder : defaultFolder, threadTitle: threadTitle, threadId: threadId});
};

determineDefaultFolder();