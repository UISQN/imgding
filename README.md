# imgding

* "imgding" is short for image-downloading. 
* is a WebExtension for Firefox or Chrome/Chromium.
* parses the active browser tab for links (href) that contain images (img)
* if the link target points to a supported image host (imagebam, imgbox, pimpandhost, imagevenue, pixhost) the linked image is downloaded
* the images are downloaded to a subfolder of the browser download directory, e.g. /home/user/Downloads/imgding/targetfolder


