
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


const LINKS = {
    _links: [],
    getLinks: () => LINKS._links,
    addLink: (index, folderName, prefixFiles, url, imagehost) => {
        let link = {
            index: index,
            state : STATE.NEW,
            folderName: folderName,
            prefixFiles: prefixFiles,
            id: Math.random().toString(36),
            link: url,
            imagehost: imagehost
        };
        LINKS._links.push(link);
    },
    updateLinkState: (link, newState, error) =>  {
        if (link.downloadTimer && (newState === STATE.OK || newState === STATE.ERROR)) {
            clearTimeout(link.downloadTimer);
            link.downloadTimer = null;
        }
        link.state = newState;
        link.error = error;
        MESSENGER.sendMessageToGui({updatelink : {link : link}});
        // asynchronous call, because we might be called from processQueue
        setTimeout(() => PROCESSOR.processQueue());
    },
    clearLinksList: () => {
        LINKS._links.forEach(l => browser.downloads.erase({id : l.downloadId}));
        LINKS._links = [];
    },
    findByDownloadId: (downloadId) => {
        return LINKS._links.find(l => l.downloadId === downloadId)
    },
    getCountProcessing: () => {
        return LINKS._links.filter(l => l.state === STATE.IN_PROGRESS).length;
    },
    getById: (linkId) => {
        return LINKS._links.find(l => l.id === linkId);
    },
    getCounts: () => {
        let error = 0;
        let ok = 0;
        LINKS._links.forEach(l => {
            switch(l.state) {
                case STATE.OK:
                    ok++;
                    break;
                case STATE.ERROR:
                    error++;
                    break;
            }
        });
        return {total: LINKS._links.length, error: error, ok: ok};
    },
    getCountProcessingOrNew: () => {
        return LINKS._links.filter(l => l.state === STATE.IN_PROGRESS || l.stage === STATE.NEW).length;
    },
    getNewLinks: (count) => {
        return LINKS._links
            .filter(l => l.state === STATE.NEW)
            .slice(0, count);
    },
    filter: (matcher) => {
        return LINKS._links.filter(l => matcher(l));
    }
};
