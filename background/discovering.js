// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


const DISCOVERING = {
    discover : (folderName, prefixFiles) => {
        browser.windows.getCurrent().then( w => {
                browser.tabs.query({active: true, windowId: w.id}).then(tabs => {
                    for (let tab of tabs) {
                        browser.tabs.executeScript(tab.id, {file: "/polyfill/browser-polyfill.min.js"})
                            .then(() => browser.tabs.executeScript(tab.id, { code: "var imgdingFolderName = '" + folderName + "'; var imgdingPrefixFiles = " + prefixFiles + ";"}))
                            .then(() => browser.tabs.executeScript(tab.id, {file: "/inject/inject.js"}))
                            .catch(x => addLog("failed to get Tab:" + x));
                    }
                });
            }
        );
    },
    handleDiscoveredLinks: (discoveredlinks) => {
        discoveredlinks.links.forEach((l, i) => {
            let imagehost = imagehosts.find(imagehost => l.match(imagehost.urlpattern));
            LINKS.addLink(i+1, discoveredlinks.folderName, discoveredlinks.prefixFiles, l, imagehost.name);
        });
        browser.runtime.sendMessage({links: LINKS.getLinks()});
        PROCESSOR.processQueue();
    }
};