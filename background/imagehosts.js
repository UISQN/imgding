// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

imagehosts = [
    {
        name : 'imagebam',
        urlpattern : '^.*imagebam\\.com\\/image.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");

            let img = doc.querySelector("img.image");

            let imageLink = null;
            if (img) {
                imageLink = img.src;
            } else {
                // fallback: if the image is not found on the pages (e.g. missing cookie)
                // ther could be a meta attribute containing the url
                let meta = doc.querySelector("meta[property='og:image']");
                imageLink = meta.getAttribute("content");
            }

            if (imageLink) {
                filename = imageLink.split("/").slice(-1)[0];
                return {
                    url : imageLink,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        }
    },
    {
        name : 'imgbox',
        urlpattern : '^.*imgbox\\.com\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img.image-content");
            if (img) {
                return {
                    url : img.src,
                    filename : img.title
                }
            }
            return null;
        }

    },
    {
        name : 'pixhost',
        urlpattern : '^.*pixhost\\.to\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#image");

            let filename = img.alt;
            if (filename.indexOf('_') > -1) {
                filename = filename.substr(filename.indexOf('_') + 1);
            }
            if (img) {
                return {
                    url : img.src,
                    filename : filename
                }
            }
            return null;
        }
    },
    {
        name : 'imagevenue',
        urlpattern : '^.*imagevenue\\.com\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#thepic");
            if (img) {
                let filename = img.src.split("/").slice(-1)[0];
                if (filename.indexOf('_') > -1) {
                    filename = filename.substr(filename.indexOf('_') + 1);
                }
                filename = filename.replace(/\B_12[23]_\d+lo(?=\..{3,4}$)/, "");
                let l = getLocation(pageUrl);
                // img.src is relative and mangled by the DOMParser(). img.alt contains the original relative path
                let imageUrl = l.protocol + "//" + l.hostname + "/" + img.alt;
                return {
                    url : imageUrl,
                    filename : decodeURIComponent(filename)
                }
            } else {
                img = doc.querySelector("div.col-md-12 img");
                if (img) {
                    return {url: img.src, filename: img.alt, status: "OK"};
                }
            }
            return null;
        }
    },
    {
        name : 'imagetwist',
        urlpattern : '^.*imagetwist\\.com\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img.pic");
            if (img) {
                let filename = img.src.split("/").slice(-1)[0];
                return {
                    url : img.src,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        }
    },
    {
        name : 'ibb',
        urlpattern : '^.*ibb\\.co\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            // use the download-link to get the original sized image
            let download = doc.querySelector("a.btn-download");
            if (download) {
                let filename = download.href.split("/").slice(-1)[0];
                return {
                    url : download.href,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        }
    },
    {
        name : 'pimpandhost',
        urlpattern : '^.*pimpandhost\\.com\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img.original");
            if (img) {
                let filename = img.src.split("/").slice(-1)[0];
                return {
                    url : img.src.startsWith("http") ? img.src : pageUrl.substr(0, pageUrl.indexOf(":")) + img.src.substring(img.src.indexOf(":")),
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        },
        fixImagePageUrl : (pageUrl) => {
            // pimpandhost URLs come in different flavors, but all contain a numeric id.
            // pimpandhost always redirects (via HTTP 30X) old format URLs to a location in the current form.
            // To avoid this redirect and make the original-size appendix work
            // the id is extracted and a new URL is buildt.
            let match = pageUrl.match(/(.*)\/([0-9]+)(.*)/);
            if (match) {
                let id = match[2];
                pageUrl = "https://pimpandhost.com/image/" + id + "?size=original";
            }
            return pageUrl;
        }
    },
    {
        name : 'turboimagehost',
        urlpattern : '^.*turboimagehost\\.com\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#imageid");
            if (!img) {
                img = doc.querySelector("img#uImage");
            }
            if (img) {
                let filename = img.src.split("/").slice(-1)[0];
                return {
                    url : img.src,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        }
    },
    {
        name : 'hotflick',
        urlpattern : '^.*hotflick\\.net\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#img");
            if (img) {
                return {
                    url : img.src,
                    filename : img.alt
                }
            }
            return null;
        }
    },
    {
        name : 'postimg',
        urlpattern : '^.*postimg\\.(org|cc)\\/.*',
        imagefunction : (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#main-image");
            if (img) {
                let filename = img.src.split("/").slice(-1)[0];
                return {
                    url : img.src,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        },
        fixImagePageUrl : (pageUrl) => {
            return pageUrl.replace("postimg.org", "postimg.cc")
        }
    },
    {
        name: 'imagefap',
        urlpattern: 'https?://(www\\.)?imagefap\\.com',
        imagefunction: (index, pageUrl, pageContent) => {
            doc = new DOMParser().parseFromString(pageContent, "text/html");
            let img = doc.querySelector("img#mainPhoto");
            if (img) {
                return {
                    url : img.src,
                    filename : img.alt
                }
            }
            return null;
        },
    },
    {
        name: 'xhamster',
        urlpattern: 'https?://xhamster\\.com/photos/gallery/',
        imagefunction: (index, pageUrl, pageContent) => {
            // FIXME: return list of images her, so downloading all pages is not necessary
            let doc = new DOMParser().parseFromString(pageContent, "text/html");
            let text = doc.querySelector("script#initials-script").innerText.replace(/.*[\s\S].*initials =/m, "");
            text = text.trim();
            text = text.replace(/;$/, "");
            let data = null;
            try {
                data = JSON.parse(text);
            } catch (e) {
                addLog(pageUrl);
                addLog("Error parsing JSON.");
                addLog(e.name + ': ' + e.message);
                addLog(pageUrl);
                addLog(text);
                return null;
            }

            let item = data.photosGalleryModel.photos.items.find(i => i.pageURL === pageUrl);
            if (item) {
                let filename = item.imageURL.split("/").slice(-1)[0];
                return {
                    url : item.imageURL,
                    filename : decodeURIComponent(filename)
                }
            }
            return null;
        },
    },
];

// count per host for 14 days:
// imagebam.com	7632
// pixhost.to	4566
// imagevenue.com	1790
// pimpandhost.com	1549
// imgbox.com	1485
// turboimagehost.com	456
// imagetwist.com	391
// hotflick.net	96

let getLocation = function(href) {
    let l = document.createElement("a");
    l.href = href;
    return l;
};
