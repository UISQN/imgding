// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


const DOWNLOADS = {
    timeout: 0,
    init: () => {
        browser.downloads.onChanged.addListener( e => {
            let link = LINKS.findByDownloadId(e.id);
            if (link) {
                let downloaditems = browser.downloads.search({ id : link.downloadId});
                if (downloaditems) {
                    downloaditems.then( x => {
                        if (x[0].state === "complete") {
                            if (x[0].error) {
                                LINKS.updateLinkState(link, STATE.ERROR, "Error-Reason: "+  x[0].error);
                            } else if(x[0].fileSize < 1024){
                                LINKS.updateLinkState(link, STATE.ERROR, "Error-Reason: File to small, only " + x[0].fileSize + " bytes." );
                                browser.downloads.removeFile(link.downloadId)
                                    .catch(() => {})
                                    .then(() => browser.downloads.erase({id : link.downloadId}))
                            } else {
                                LINKS.updateLinkState(link, STATE.OK);
                            }
                            browser.downloads.erase({id : link.downloadId});
                        } else if (x[0].state === "interrupted") {
                            LINKS.updateLinkState(link, STATE.ERROR, "Interrupted" + (x[0].error ?  ", Reason: " +  x[0].error : ""));
                            browser.downloads.erase({id : link.downloadId});
                        }
                    })
                }
            }
        })
    },
    download: (link, url, filename) => {
        let download = {
            url : url,
            filename: filename
        };
        browser.downloads.download(download).then(
            downloadId => {
                link.downloadId = downloadId;
                link.downloadTimer = DOWNLOADS.createDownloadTimer(link);
            },
            x => LINKS.updateLinkState(link, STATE.ERROR, x.message)
        );
    },
    createDownloadTimer: (link) => {
        if (DOWNLOADS.timeout >= 120) {
            return setTimeout(() => DOWNLOADS.timout(link),   timeout * 1000)
        } else {
            return null;
        }
    },
    timout: (link) => {
        addLog("Timeout: " + link.link);
        browser.downloads.erase({id : link.downloadId});
        LINKS.updateLinkState(link, STATE.ERROR, "timeout");
    },
    cancelDownload: (linkId) => {
        let l = LINKS.getById(linkId);
        if (l && l.state !== STATE.OK) {
            browser.downloads.erase({id: l.downloadId}).then(() => {
                LINKS.updateLinkState(l, STATE.ERROR, "canceled");
            });
        }
    }
};