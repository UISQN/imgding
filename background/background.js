
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.


let log = [];
let uiState = {
    showSuccessful: true,
    showNew: true,
    showError: true,
    showInProgress: true
};

const STATE = Object.freeze({
    NEW:   "new",
    IN_PROGRESS:  "inProgress",
    OK: "ok",
    ERROR: "error"
});

init = () => {
    browser.runtime.onMessage.addListener((message) => {
        if (message.command) {
            if (message.command === "send.data") {
                MESSENGER.guiVisible = true;
                MESSENGER.sendMessageToGui({data : {links : LINKS.getLinks(), log: log, uiState: uiState}});
            } else if (message.command === "gui.closed") {
                MESSENGER.guiVisible = false;
            } else if (message.command === "clear") {
                clear();
            } else if (message.command === "retryFailed") {
                PROCESSOR.retryFailed();
            } else if (message.command === "reloadPreferences") {
                loadPreferences();
            }
        } else if (message.uiState) {
            uiState = message.uiState
        } else if (message.discoveredlinks) {
            DISCOVERING.handleDiscoveredLinks(message.discoveredlinks);
        } else if (message.download) {
            clear();
            let folderName = message.download.foldername;
            let prefixFiles = message.download.prefixFiles;
            if (!folderName) {
                folderName = new Date().toISOString().replace(/:/g, "-");
            }
            DISCOVERING.discover(folderName, prefixFiles);
        } else if (message.cancelDownload) {
            DOWNLOADS.cancelDownload(message.cancelDownload);
        } else if (message.suckThread) {
            clear();
            let threadId = message.suckThread.threadId;
            let folderName = message.suckThread.foldername;
            let pageFrom = message.suckThread.pageFrom;
            let pageTo = message.suckThread.pageTo;
            let threadUrl = message.suckThread.url;
            if (!folderName) {
                folderName = new Date().toISOString().replace(/:/g, "-");
            }
            suckThread(threadId, threadUrl, pageFrom ,pageTo, folderName);
        }
    });
    DOWNLOADS.init();
    loadPreferences();
};

const loadPreferences = () => {
    let storageItem = browser.storage.local.get('maxparallel');
    storageItem.then((res) => {
        PROCESSOR._maxProcessing = res.maxparallel || 5;
    });
    storageItem = browser.storage.local.get('timeout');
    storageItem.then((res) => {
        DOWNLOADS.timeout = res.timeout || 0;
    });
};

clear = () => {
    LINKS.clearLinksList();
    PROCESSOR.clear();
    browser.browserAction.setIcon({path: "icons/imgding-48.png"});
};


// FIXME: code duplication (inject.js)
isImagehost = (url) => {
    return url.match('^.*hotflick\\.net\\/.*')
        || url.match('^.*imagebam\\.com\\/image.*')
        || url.match('^.*imgbox\\.com\\/.*')
        || url.match('^.*pixhost\\.to\\/.*')
        || url.match('^.*imagevenue\\.com\\/.*')
        || url.match('^.*imagetwist\\.com\\/.*')
        || url.match('^.*ibb\\.co\\/.*')
        || url.match('^.*pimpandhost\\.com\\/.*')
        || url.match('^.*turboimagehost\\.com\\/.*')
        || url.match('^.*postimg\\.(org|cc)\\/.*')
};

suckThread = (threadId, url, pageFrom ,pageTo, folderName) => {
    let match = url.match(/(https?:\/\/.*\/).*$/);
    for (let p = pageFrom; p <= pageTo; p++) {
        let pageUrl = match[1] + "t" + threadId + "-p" + p + "-x.html";
        addLog("Suck Thread Page: " + pageUrl);
        fetch(pageUrl,  {method: 'GET', credentials: 'include'})
            .then(response => response.text())
            .then(pageContent => {
                doc = new DOMParser().parseFromString(pageContent, "text/html");
                let candidates = Array.from(doc.querySelectorAll("a img"));
                let links = candidates
                    .filter(c => c.parentNode.href && isImagehost(c.parentNode.href))
                    .map(c => c.parentNode.href);

                DISCOVERING.handleDiscoveredLinks({links: links, prefixFiles: true, folderName: folderName + " - " + p});
            }).catch((reason)=> addLog("SuckThread failed: " + reason));
    }
};


addLog = (l) => {
    l = new Date().toISOString() + " " + l;
    log.unshift(l);
    log = log.slice(0, 100);
    browser.runtime.sendMessage({log: l});
};

init();

