#!/usr/bin/env bash

VERSION=$(cat version)
echo Setting version $VERSION

awk -v version=$VERSION '/"version"/ {print "  \"version\": \"" version "\","} !/"version"/ {print}' manifest.json > manifest.json.new
mv manifest.json.new manifest.json

awk -v version=$VERSION '/VERSION/ {print "<!-- VERSION --> " version} !/VERSION/ {print}' popup/imgding.html > popup/imgding.html.new
mv popup/imgding.html.new popup/imgding.html
